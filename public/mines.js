{

	function minesgame ( el ) {

		minesgame.gameobjects.push(this);
		// 8x8 16x16 30x16

		this.props = {
			height: 16,
			width: 30,
			mines: 99,
			status: 'playing',
			flags: 0,
			revealed : 0,
			total : 0,
			seconds: null
		};
		this.elements={
			container : typeof el != 'undefined' ? el : document.createElement('div'),
			controlsninfo : document.createElement('div'),
			btncontainer : document.createElement('div'),
			tmrelement : document.createElement('div'),
			flagselement : document.createElement('div'),
			flagslabel : document.createElement('div'),
			flagscontainer : document.createElement('div'),
			statustext : document.createElement('div'),
			newgamebtn : document.createElement('button'),
			playpausebtn : document.createElement('button'),
			rows : [],
			btns : []
		};

		this.onaddflag = [];
		this.onboxreveal = [];
		this.onhitmine = [];
		this.onremoveflag = [];
		this.ongameend = [];
		this.onaddqmark = [];
		this.onremoveqmark = [];

		function cntmines ( btn ) {
			var self = btn.dataMinesObject;
			var ret = 0;
			if ( btn.previousElementSibling !== null ) {
				if ( btn.previousElementSibling.dataValue === 'B' ) ret ++;
				if ( btn.dataPos.y > 0
					 && self.elements.rows[ btn.dataPos.y - 1 ]
					 .children[btn.dataPos.x - 1]
					 .dataValue === 'B' ) ret ++;
				if ( btn.dataPos.y < (self.props.height - 1)
					 && self.elements.rows[ btn.dataPos.y + 1 ]
					 .children[btn.dataPos.x - 1]
					 .dataValue === 'B' ) ret ++;
			}
			if ( btn.nextElementSibling !== null ) {
				if ( btn.nextElementSibling.dataValue === 'B' )ret ++;
				if ( btn.dataPos.y > 0
					 && self.elements.rows[ btn.dataPos.y - 1 ]
					 .children[btn.dataPos.x + 1]
					 .dataValue === 'B' ) ret ++;
				if ( btn.dataPos.y < (self.props.height - 1)
					 && self.elements.rows[ btn.dataPos.y + 1 ]
					 .children[btn.dataPos.x + 1]
					 .dataValue === 'B' ) ret ++;
			}
			if ( btn.dataPos.y > 0
				 && self.elements.rows[ btn.dataPos.y - 1 ]
				 .children[ btn.dataPos.x]
				 .dataValue === 'B' ) ret ++;
			if ( btn.dataPos.y < (self.props.height - 1)
				 && self.elements.rows[ btn.dataPos.y + 1 ]
				 .children[btn.dataPos.x]
				 .dataValue === 'B' ) ret ++;

			return ret;
		}
		
		this.revealAll = function () {
			for ( var i = 0 ; i < this.elements.btns.length ; i++ ) {
				if ( this.elements.btns[i].dataValue == 'B' ) {
					if ( this.elements.btns[i].classList.contains('flagflag') &&
						 ! this.elements.btns[i].classList.contains('hasmine') ) {
						this.elements.btns[i].className = this.elements.btns[i].className.replace('flagflag','opened');
					} else this.elements.btns[i].className = this.elements.btns[i].className.replace( /flag(false|none)/, 'opened' );
				} else if ( this.elements.btns[i].classList.contains('flagflag') ) {
					this.elements.btns[i].className = this.elements.btns[i].className.replace('flagflag','flagfalse');
				}
			}
			return this;
		}

		this.reveal = function ( btn ) {
			if ( this.props.status != 'playing' ) return;
			if ( btn.className.indexOf('opened')>-1) return;
			if ( btn.className.indexOf('flagflag') !== -1 || btn.className.indexOf('flagqmark') !== -1 ) return;

			if ( btn.dataValue != 'B' ) {

				btn.className += ' opened';

				if ( btn.dataValue != '0' )
					btn.value = btn.dataValue;
				else if ( btn.dataValue == '0' ) {

					if ( btn.previousElementSibling !== null) this.reveal ( btn.previousElementSibling );

					if ( btn.nextElementSibling !== null) this.reveal ( btn.nextElementSibling );

					if ( btn.parentElement.previousElementSibling !== null ) {
						var cbtn = btn.parentElement.previousElementSibling.children[btn.dataPos.x];
						this.reveal ( cbtn );
						if ( cbtn.previousElementSibling !== null) this.reveal ( cbtn.previousElementSibling );
						if ( cbtn.nextElementSibling !== null) this.reveal ( cbtn.nextElementSibling );
					}

					if ( btn.parentElement.nextElementSibling !== null ) {
						var cbtn = btn.parentElement.nextElementSibling.children[btn.dataPos.x];
						this.reveal ( cbtn );
						if ( cbtn.previousElementSibling !== null) this.reveal ( cbtn.previousElementSibling );
						if ( cbtn.nextElementSibling !== null) this.reveal ( cbtn.nextElementSibling );
					}

				}

				this.props.revealed ++;

				if ( this.props.revealed + this.props.mines == this.props.total ) this.win();

			} else if ( btn.dataValue == 'B' ) {
				this.revealAll();
				btn.className += ' explode';
				this.fireEvent('hitmine',{
					"game":this,
					"target":btn,
					"box":btn
				});
				this.lost();
			}

			this.fireEvent('boxreveal',{
				"game":this,
				"target":btn,
				"box":btn
			});

			return this;
		}

		this.reset = function () {

			this.elements.container.classList.add('status-stopped');
			this.elements.container.classList.remove('game-won');
			this.elements.container.classList.remove('game-lost');
			this.elements.container.classList.remove('status-playing');
			this.elements.container.classList.remove('status-end');
			this.elements.container.classList.remove('status-paused');

			this.props.revealed = 0;
			this.props.flags = 0;
			this.props.total = this.elements.btns.length;

			for ( var i = 0 ; i < this.elements.btns.length; i ++ ) {
				this.elements.btns[i].dataValue = '0';
				this.elements.btns[i].dataFlag = 'none';
				this.elements.btns[i].className = 'lvzminesbtn flagnone';
				this.elements.btns[i].value = '';
			}

			for ( var i = 0 ; i < this.props.mines; i ++ ) {

				var j = 0;
				while ( j < 100 ) {
					j ++;
					var posmine =  Math.floor ( Math.random() * this.elements.btns.length );
					if ( this.elements.btns[posmine].dataValue != 'B' ) {
						this.elements.btns[posmine].dataValue = 'B';
						this.elements.btns[posmine].className += ' hasmine';
						break;
					}
				}
			}

			for ( var i = 0 ; i < this.elements.btns.length; i ++ ) {
				if ( this.elements.btns[i].dataValue != 'B' )
					this.elements.btns[i].dataValue = cntmines ( this.elements.btns[i] );
			}

			// this.play();
			this.pause();

			return this;
		}

		this.addEvent = function ( e, f ) {
			if (typeof this['on'+e] == 'undefined' ) return false;
			for ( var i=0; i<this['on'+e].length; i++)if(this['on'+e][i]===f)return this;
			this['on'+e].push(f);
			return this;
		}

		this.removeEvent=function ( e, f ) {
			if (typeof this['on'+e] == 'undefined' ) return false;
			if (typeof f=='undefined') {
				this['on'+e]=[];
			} else {
				for ( var i = 0; i<this['on'+e].length; i++ ) if ( this['on'+e][i] === f)this['on'+e].splice(i,1);
			}
			return this;
		}

		this.fireEvent = function ( e, obj ) {
			if ( typeof this['on'+e] == 'undefined' ) return false;
			obj.type = e;
			for ( var i=0; i<this['on'+e].length; i++ ) {
				this['on'+e](obj);
			}
			return this;
		}

		this.play = function () {
			this.props.status = 'playing';
			this.elements.container.classList.add('status-playing');
			this.elements.container.classList.remove('status-stopped');
			this.elements.container.classList.remove('status-end');
			this.elements.container.classList.remove('status-paused');
			if (minesgame.tmr===false) minesgame.tmr = window.setInterval( minesgame.tmrf, 1000 );
			return this;
		}

		this.pause = function () {
			this.elements.container.classList.remove('status-playing');
			this.elements.container.classList.remove('status-stopped');
			this.elements.container.classList.remove('status-end');
			this.elements.container.classList.add('status-paused');
			this.props.status = 'paused';
			return this;
		}

		this.end = function () {
			// this.props.status = 'end';
			this.elements.container.classList.remove('status-playing');
			this.elements.container.classList.add('status-stopped');
			this.elements.container.classList.add('status-end');
			this.elements.container.classList.remove('status-paused');
			this.fireEvent('gameend',{
				"game":this,
				"target":this
			});
			return this;
		}

		this.win = function () {
			this.props.status = 'win';
			this.elements.statustext.innerHTML = 'WIN!';
			this.elements.container.classList.add('game-won');
			this.elements.container.classList.remove('game-lost');
			this.fireEvent('gamewin',{
				"game":this,
				"target":btn,
				"box":btn
			});
			this.end();
			return this;
		}

		this.lost = function () {
			this.props.status = 'lost';
			this.elements.container.classList.remove('game-won');
			this.elements.container.classList.add('game-lost');
			this.elements.statustext.innerHTML = 'LOST';
			this.fireEvent('gamelost',{
				"game":this,
				"target":btn,
				"box":btn
			});
			this.end();
			return this;
		}

		for (var iy = 0; iy < this.props.height; iy++ ) {
			var row = document.createElement('div');
			row.className = 'lvzminesrow';
			row.dataMinesObject = this;
			this.elements.rows.push(row);
			this.elements.btncontainer.appendChild(row);

			for ( var ix = 0; ix < this.props.width; ix++ ) {
				var btn = document.createElement('input');
				btn.className = 'lvzminesbtn';
				btn.type = 'button';
				btn.value='';
				this.elements.btns.push(btn);
				row.appendChild(btn);
				btn.dataMinesObject = this;
				btn.dataValue = '0';
				btn.dataPos = { x : ix, y : iy };

				btn.addEventListener ( 'click', function ( e ) {
					e.preventDefault();
					var self = e.target.dataMinesObject;

					if ( self.props.status == 'paused' ) {
						// return false;
						self.play();
					}
					else if ( self.props.status == 'win' || self.props.status == 'lost' ) return false;
					//if ( self.props.status != 'playing' ) 
					self.reveal(e.target);
					return false;
				});

				btn.addEventListener ( 'contextmenu', function ( e ) {
					e.preventDefault();
					var self = e.target.dataMinesObject;
					if ( self.props.status != 'playing' ) return false;
					if ( e.target.className.indexOf ( 'opened' ) < 0) switch ( e.target.dataFlag ) {

						case 'flag':
						e.target.className = e.target.className.replace(/ flagflag/g,' flagqmark');
						e.target.dataFlag = 'qmark';
						self.props.flags--;
						self.elements.flagselement.innerHTML = self.props.flags + '/' + self.props.mines;
						self.fireEvent('removeflag',{
							"game":self,
							"target":e.target,
							"box":e.target,
							"flags":self.props.flags
						});
						self.fireEvent('addqmark',{
							"game":self,
							"target":e.target,
							"box":e.target,
							"flags":self.props.flags
						});
						break;

						case 'qmark':
						e.target.className = e.target.className.replace(/ flagqmark/g,'');
						e.target.dataFlag = 'none';
						self.fireEvent('removeqmark',{
							"game":self,
							"target":e.target,
							"box":e.target,
							"flags":self.props.flags
						});
						break;

						default: case 'none':
						if ( e.target.className.match( /flag(flag|qmark|false|none)/ ) === null)
							e.target.className += ' flagflag';
						else e.target.className = e.target.className.replace( /flag(flag|qmark|false|none)/, 'flagflag' );
						e.target.dataFlag = 'flag';
						self.props.flags++;
						self.elements.flagselement.innerHTML = self.props.flags + '/' + self.props.mines;
						self.fireEvent('addflag',{
							"game":self,
							"target":e.target,
							"box":e.target,
							"flags":self.props.flags
						});
						break;

					}

					return false;
				});

			}
		}

		this.elements.controlsninfo.dataMinesObject = this;
		this.elements.controlsninfo.className = 'controls';
		this.elements.statustext.dataMinesObject = this;
		this.elements.statustext.className = 'statustext';
		this.elements.flagselement.dataMinesObject = this;
		this.elements.flagselement.className = 'flagscounter';
		this.elements.flagselement.innerHTML = '0/'+this.props.mines;
		this.elements.flagslabel.className = 'flagslabel';
		this.elements.flagscontainer.className = 'flagscontainer';
		this.elements.tmrelement.dataMinesObject = this;
		this.elements.tmrelement.className = 'gametime';

		this.elements.newgamebtn.dataMinesObject = this;
		this.elements.newgamebtn.className = 'newgamebtn';
		this.elements.newgamebtn.addEventListener('click', function ( e ) {
			var self = e.target.dataMinesObject;
			self.reset();
			self.props.seconds = 0;
			self.props.status = 'playing';
			self.elements.statustext.innerHTML = '';
			self.props.flags=0;
			self.elements.flagselement.innerHTML = '0/'+self.props.mines;
			self.pause();
		});

		this.elements.playpausebtn.dataMinesObject = this;
		this.elements.playpausebtn.className = 'playpausebtn';
		this.elements.playpausebtn.addEventListener('click', function( e ) {
			var self = e.target.dataMinesObject;
			if ( self.props.status === 'lost' || self.props.status === 'win' ) {
				self.reset();
				self.props.seconds = 0;
				self.props.status = 'playing';
				self.elements.statustext.innerHTML = '';
				self.props.flags=0;
				self.elements.flagselement.innerHTML = '0/'+self.props.mines;
				self.pause();
			}
			else if ( self.props.status === 'paused' ) {
				self.play();
			}
			else {
				self.pause();
			}
		});

		this.elements.container.appendChild( this.elements.controlsninfo );
		this.elements.controlsninfo.appendChild( this.elements.newgamebtn );
		this.elements.controlsninfo.appendChild( this.elements.playpausebtn );

		this.elements.flagscontainer.appendChild(this.elements.flagslabel);
		this.elements.flagscontainer.appendChild(this.elements.flagselement);

		this.elements.controlsninfo.appendChild( this.elements.flagscontainer );
		this.elements.controlsninfo.appendChild( this.elements.tmrelement );
		this.elements.controlsninfo.appendChild( this.elements.statustext );
		
		this.elements.container.appendChild(this.elements.btncontainer);
		this.elements.btncontainer.dataMinesObject = this;
		this.elements.container.dataMinesObject = this;

		this.reset();
	}

	minesgame.onload = function () {
		var mel = document.getElementsByClassName('mines');
		for (var i=0; i<mel.length; i++)
			if ( typeof mel[i].dataMinesObject == 'undefined' )
			(new minesgame(mel[i]));
	};

	minesgame.loadApp = function ( app ) {

		var mgame=new minesgame();

		var self = lvzwebdesktop(app.container);
		var cont = app.container;

		if ( self === false ) {
			cont.appendChild(mgame.elements.container);
		}
		else {
			self.minesgame = mgame;

			if ( self.type == 'window' ) {

				self.attachItem(mgame.elements.container)
					.addEvent('itemattached', function ( e ) {
						e.target.minesgame = e.item.dataMinesObject;
					})
					.setWidth(mgame.props.width * 34)
					.setHeight(mgame.props.height * 34 +  85)
					.setLayout('menu:minimize,close')
					.disableResize()
					.resizeOnContent(false)
				;
				self.props.maximizable = false;
				self.props.resizable = false;
				self.props.disableBorder = true;

				minesgame.buildToolbar(mgame, self);
			}
		}
	};

	minesgame.buildToolbar = function (m,w) {

		var t = null;
		if ( typeof w.toolbar == 'object' && w.toolbar !== null )
			t = w.toolbar;
		else 
			t = new lvzwebdesktop.ToolbarObject ();

		var s = null;

		if ( typeof w.statusbar == 'object' && w.statusbar !== null )
			s = w.statusbar;
		else
			s = new lvzwebdesktop.StatusbarObject();

		t.setEditable(true );

		var tg1 = new lvzwebdesktop.ToolbarGroupObject();
		var tg2 = new lvzwebdesktop.ToolbarGroupObject();
		
		tg1.addItem(m.elements.playpausebtn);
		tg2.addItem(m.elements.tmrelement);

		t.addItem(tg1).addItem(tg2);

		m.elements.newgamebtn.style.display = 'none';
		m.elements.controlsninfo.style.display = 'none';

		t.attachToWindow ( w );

		s.attachToWindow ( w );

		s
			.addItem(m.elements.statustext)
			.addItem(m.elements.flagscontainer)
		;

		var mb = new lvzwebdesktop.MenubarObject();

		var gmenu = mb.createMenuItem('Game');
		var vmenu = mb.createMenuItem('View');
		var pmenu = mb.createMenuItem('Preferences');
		var hmenu = mb.createMenuItem('Help');

		var gsmenu = new lvzwebdesktop.MenuObject();
		gsmenu.addItem("Exit", function ( e ) {
			console.log ( e ) ;
		});

		mb.addSubmenu(gsmenu,gmenu);

		hmenu.style.cssFloat = 'right';

		mb.addMenuItem(gmenu)
			.addMenuItem(vmenu)
			.addMenuItem(pmenu)
			.addMenuItem(hmenu)
		;

		w.attachItem(mb, 'top' );

	};

	minesgame.gameobjects = [];

	minesgame.tmr = false;

	minesgame.tmrf = function () {
		var a = minesgame.gameobjects.length;
		for (var i = 0 ; i < minesgame.gameobjects.length; i++ ) {
			if ( minesgame.gameobjects[i].props.status == 'playing' ) {
				minesgame.gameobjects[i].props.seconds ++;
				minesgame.gameobjects[i].elements.tmrelement.innerHTML = minesgame.gameobjects[i].props.seconds;
			} else a--;
		}

		if (a==0) {
			clearInterval(minesgame.tmr);
			minesgame.tmr = false;
		}
	}

	window.addEventListener('load',function () {
		minesgame.onload();
	});

	var url = new URL(document.currentScript.src);
	var c = url.searchParams.get("$");
	if ( c !== null ) window[c].minesgame = minesgame;
	else window.minesgame = minesgame;
}
