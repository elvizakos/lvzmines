# LVz Mines Game #

Just an other minesweeper game I made for fun.

Demo at https://elvizakos.gitlab.io/lvzmines

## minesgame object ##

### Properties ###

  * props.height
  * props.width
  * props.mines
  * props.status
  * props.flags
  * props.revealed
  * props.total
  * props.seconds
  * elements.container
  * elements.btncontainer
  * elements.tmrelement
  * elements.flagselement
  * elements.statustext
  * elements.rows
  * elements.btns

### Methods ###

  * revealAll
  * reveal
  * reset
  * addEvent
  * removeEvent
  * fireEvent
  * play
  * pause
  * end

### Events ###

  * addflag
  * boxreveal
  * hitmine
  * removeflag
  * gameend
  * addqmark
  * removeqmark
  * gamewin
  * gamelost

## game html element ##

### Attributes ###
